var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe('Bicicleta API', ()=>{
    describe('GET Bicicletas /', ()=>{
        it('Status 200', () =>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var b1 = new Bicicleta(1, 'Rojo', 'Urbana', [-34.6012424, -58.3861497]);
            Bicicleta.add(b1);

            request.get('http://localhost:3000/api/bicicletas', function(error,response,body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
    

    describe('POST Bicicletas /create', ()=>{
        it('Status 200', (done) =>{
            var headers = {'content-type': 'application/json'};
            var b1 = '{"id":10, "color":"Rojo", "modelo":"Urbana", "lat": -34.6012424, "lng": -58.3861497}';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: b1
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("Rojo");
                done();
            });

        });
    });


    describe('POST Bicicletas /update', ()=>{
        it('Status 200', (done) =>{
            var b1 = new Bicicleta(1, 'Rojo', 'Urbana', [-34.6012424, -58.3861497]); //Creo las bicicletas para tener datos a editar
            var b2 = new Bicicleta(2, 'Azul', 'Urbana', [-34.596932, -58.3808287]); //Creo las bicicletas para tener datos a editar
            Bicicleta.add(b1);
            Bicicleta.add(b2); 
            var headers = {'content-type': 'application/json'};
            var b1 = '{"id":2, "color":"Negro", "modelo":"Urbana", "lat": -34.6012424, "lng": -58.3861497}';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/update',
                body: b1
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(2).color).toBe("Negro");
                done();
            });

        });
    });

    // describe('DELETE Bicicletas /delete', ()=>{
    //     it('Status 204', (done) =>{
    //         var b1 = new Bicicleta(1, 'Rojo', 'Urbana', [-34.6012424, -58.3861497]); //Creo las bicicletas para tener datos a editar
    //         var b2 = new Bicicleta(2, 'Azul', 'Urbana', [-34.596932, -58.3808287]); //Creo las bicicletas para tener datos a editar
    //         Bicicleta.add(b1);
    //         Bicicleta.add(b2); 
    //         request.delete({
    //             url: 'http://localhost:3000/api/bicicletas/delete',
    //             body: '"id":2'
    //         }, function(error, response, body){
    //             expect(response.statusCode).toBe(204);
    //             expect(Bicicleta.allBicis.length).toBe(1);
    //             done();
    //         });

    //     });
    // });
});
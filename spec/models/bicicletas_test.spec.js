var Bicicleta = require('../../models/bicicleta');
beforeEach(()=>{Bicicleta.allBicis = [];});
describe('Bicicleta.allBicis', () =>{
    it('Comienza vacio', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', ()=>{
    it('Agrega una nueva bici', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var b1 = new Bicicleta(1, 'Rojo', 'Urbana', [-34.6012424, -58.3861497]);
        Bicicleta.add(b1);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(b1);
    });
});


describe('Bicicleta.findById', ()=>{
    it('Busca la bicicleta con ID 1', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var bici = new Bicicleta(1, 'Azul', 'Urbana');
        var bici2 = new Bicicleta(2, 'Verde', 'Montaña');
        Bicicleta.add(bici);
        Bicicleta.add(bici2);
        
        targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(bici.color);
        expect(targetBici.modelo).toBe(bici.modelo);
    });
});


describe('Bicicleta.removeById', ()=>{
    it('Elimina la bicicleta con ID 1', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var bici = new Bicicleta(1, 'Azul', 'Urbana');
        var bici2 = new Bicicleta(2, 'Verde', 'Montaña');
        Bicicleta.add(bici);
        Bicicleta.add(bici2);
        expect(Bicicleta.allBicis.length).toBe(2);
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1);
        // expect(Bicicleta.allBicis[0].id).toBe(!1);
    });
});